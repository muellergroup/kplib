# Release Note

## Version 1.1.1
1. Fix bugs in MANIFEST.in causing header files to be excluded in the source distribution on PyPI.
## Version 1.1.0 -- 2023.04.08
Changes to the Python interface:
1. Reduced dependencies on external packages. Use `Spglib` to get symmetry operations, aligning
   with the C++ library. Now the core functionality of `kpLib` requires only `Spglib`. For the CLI
   `kpgen`, `pymatgen` is still required and is used as a parser.
2. Changed the signature of the main function of the library: `kplib.interface::get_kpoints`.
   Rather than requiring a `pymatgen.core::Structure`, the function now defines a structure using three
   arguments:  `lattice`, `fractional_coords` and `atomic_numbers`. They can be in the format of
   either `numpy.ndarray` or plain Python `list`.
3. Comply with Python name convention: use snake_case for all arguments in `kplib.interface::get_kpoints`: `minDistance` -> `min_distance`, `minTotalKPoints` -> `min_total_kpoints`.

Bug fixes:
1. Resolved the problem of program hanging for monoclinic and trigonal system in hR representation.

Code improvements:
1. Sigificant improvement of the README.md. Add an example of a tetragonal structure to
explain the `min_distance` concept.

## Version 1.0.0 -- 2019.09.17
Main features:
1. Implemented dynamic generation scheme described in the pre-print: [Algorithms for the Generation of Generalized Monkhorst-Pack Grids](https://arxiv.org/abs/1907.13610).
2. The algorithms implemented in this version are consistent with the ones used to generate the database for version `2019.08.01` of the *K*-Point Grid Generator and the *K*-Point Server.
3. When specifying MINDISTANCE, kpLib can generate optimized grids in 0.19 seconds at 50 angstroms and within 5 seconds at 100 angstroms, on average over 102 structures randomly selected from Inorganic Crystal Structure Database (ICSD).
4. Wrapped kpLib in a Python module `kpGen` verion `0.0.1`. Thanks for the work by Shyam!
5. Added a flag `USESCALEFACTOR` to control whether or not scale factor technique is used. `TRUE` will activate it, while `FALSE` and the default behavior is to turn it off. When scale factor is used, the explicit search limits are 729, 1728, 46656 and 5632 for triclinic, monoclinic, cubic and other crystal systems, respectively. Above these limits, scale factor will be used to accelerate the generation. Grids using scale factor will have slightly higher number of symmetrically irreducible *k*-point, since kpLib doesn't perform an exhaustive search in this case.
