from pymatgen.io.vasp.inputs import Kpoints
import sys
import re

"""
A simple script to compare equivalency of two k-points file of the VASP KPOINTS format.
For now, only weights are explicitly check. The coordinate-by-cooridnate comparison 
between K-points need to be implemented in future.
"""

# Constant
PATTERN_MIN_DISTANCE = "Actual minimum periodic distance is (.*) Angstroms."
PATTERN_TOTAL_KPOINTS = "has ([0-9]+) total points."
SYMPREC = 1E-5

def compare_kpoints(file1, file2):
    kpoints_1 = Kpoints.from_file(file1)
    kpoints_2 = Kpoints.from_file(file2)

    minDistance_1 = float(re.search(PATTERN_MIN_DISTANCE, kpoints_1.as_dict()["comment"]).group(1))
    minDistance_2 = float(re.search(PATTERN_MIN_DISTANCE, kpoints_2.as_dict()["comment"]).group(1))
    num_total_kpoints_1 = int(re.search(PATTERN_TOTAL_KPOINTS, kpoints_1.as_dict()["comment"]).group(1))
    num_total_kpoints_2 = int(re.search(PATTERN_TOTAL_KPOINTS, kpoints_2.as_dict()["comment"]).group(1))
    
    # Sort weights, to make sure the k-points are equivalent up to the list of k-point stars.
    weights1 = sorted(kpoints_1.kpts_weights)
    weights2 = sorted(kpoints_2.kpts_weights)

    assert abs(minDistance_1 - minDistance_2) < SYMPREC
    assert num_total_kpoints_1 == num_total_kpoints_2

    assert len(kpoints_1.kpts) == len(kpoints_2.kpts)
    assert all([w1 == w2 for w1, w2 in zip(weights1, weights2)])
    print("Passed.")


if __name__ == "__main__":
    compare_kpoints(sys.argv[1], sys.argv[2])