import os
import pytest
import numpy as np
import math

import spglib
from pymatgen.core import Structure
from pymatgen.symmetry.analyzer import SpacegroupAnalyzer

TEST_DIR = os.path.dirname(__file__)
TEST_SYSTEMS = [
                    "cubic", "hexagonal", "trigonal_P-3m1_hP", "trigonal_R3c_hP", "trigonal_R3c_hR", 
                    "tetragonal", "orthorhombic", "monoclinic", "triclinic"
                ]
SYMPREC = 1E-5
ANGLEPREC = 1E-3

@pytest.mark.parametrize("system", TEST_SYSTEMS)
def test_spglib_equivalnce_with_pymatgen(system):
    structure = Structure.from_file(os.path.join(TEST_DIR, "POSCAR_{:s}".format(system)))
    pymatgen_spg_analyzer = SpacegroupAnalyzer(structure, symprec=SYMPREC)
    spglib_cell = (structure.lattice.matrix, structure.frac_coords, structure.atomic_numbers)
    spglib_dataset = spglib.get_symmetry_dataset(spglib_cell, symprec = 1e-5)

    pymatgen_conv_lattice = pymatgen_spg_analyzer.get_conventional_standard_structure().lattice.matrix
    spglib_conv_lattice = spglib_dataset["std_lattice"]

    ## Check the spglib and pymatgen get the same space group:
    assert pymatgen_spg_analyzer.get_point_group_symbol() == spglib_dataset["pointgroup"]
    assert pymatgen_spg_analyzer.get_space_group_number() == spglib_dataset["number"]
    assert pymatgen_spg_analyzer.get_space_group_symbol() == spglib_dataset["international"]

    ## Check the equivalence of the conventional lattice matrix:
    if 143 <= spglib_dataset["number"] <= 194:
        # For trigonal cystal system, pymatgen always transfer the conventional lattice to a 
        # hexagonal lattice. And pymatgen slightly rotate the a and b vectors to let them
        # lie on the two sides of positive x-axis, while spglib always set the a vector 
        # along the positive x-axis and let the b vector in the four quadrant of the x-y plane.
        # See: 
        # https://spglib.readthedocs.io/en/latest/definition.html#spglib-conventions-of-standardized-unit-cell
        # and the pymatgen source code on SpacegroupAnalyzer.
      
        v1, v2 = pymatgen_conv_lattice[0:2]
        u1, u2 = spglib_conv_lattice[0:2]

        # 1. Check the volume of conventional unit cell
        # 2. Check the c vector is the same
        # 3. Check the degree between a and b vectors are the same. So they are equivalent up to
        #    to arbitrary rotation.
        assert abs(np.linalg.det(pymatgen_conv_lattice) - np.linalg.det(spglib_conv_lattice)) <= SYMPREC
        assert np.all(np.isclose(pymatgen_conv_lattice[2], spglib_conv_lattice[2], atol = SYMPREC))
        degree1 = math.degrees(math.acos(np.dot(v1, v2) / (np.linalg.norm(v1) * np.linalg.norm(v2))))
        degree2 = math.degrees(math.acos(np.dot(u1, u2) / (np.linalg.norm(u1) * np.linalg.norm(u2))))
        assert abs(degree1 - degree2) <= ANGLEPREC
    elif 3 <= spglib_dataset["number"] <= 15: # Monoclinic lattices
        # The spglib and pymatgen convention are equal upto a selection of a and c vectors.
        for v, u in zip(pymatgen_conv_lattice, spglib_conv_lattice):
            assert abs(np.linalg.norm(v) - np.linalg.norm(u)) <= SYMPREC
    else:
        assert np.all(np.isclose(pymatgen_conv_lattice, spglib_conv_lattice, atol = SYMPREC))

    ## Check the equivalence of rotational symmetry operations:
    assert np.all(np.equal(
        np.array([op.rotation_matrix for op in pymatgen_spg_analyzer.get_point_group_operations()]),
        np.array(spglib_dataset["rotations"])
    ))
